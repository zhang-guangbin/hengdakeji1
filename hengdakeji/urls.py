"""hengdakeji URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from hengdakeji_app import views
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
                  path('admin/', admin.site.urls),
                  path('', views.home, name="home"),
                  path('home/', views.home, name="home"),
                  path('science/', views.science, name='science'),
                  path('about/<int:type>/', views.about, name='about'),
                  path('product/<int:type>/', views.product, name='product'),
                  path('product_detail/<int:id>/', views.product_detail, name='product_detail'),
                  path('service/<int:type>/', views.service, name='service'),
                  path('news/<int:type>/', views.news, name='news'),
                  path('news_detail/<int:news_id>/', views.news_detail, name='news_detail'),
                  path('contact/<int:type>/', views.contact, name='contact'),
                  path('ueditor/', include('DjangoUeditor.urls')),

              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
