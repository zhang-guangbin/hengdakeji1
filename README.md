# hengdakeji1

#### 介绍
这个是一个网站设计的基础，里面包含很前端网站设计和后端数据管理

#### 软件架构
软件架构说明
Python Django Pycharm 专业版 2018


#### 安装教程
建议使用Pycharm创建Django 虚拟项目，相对方便一点
Pycharm安装Django
1.  pip install django==2.2.4
2.  pip install qrcode pillow pyquery
3.  pip install whoosh django-haystack jieba
4.  pip install django-widget-tweaks docxtpl  opencv_python  requests  DjangoUeditor
5.  pip install six

#### 使用说明

1.  直接运行整个项目，不要运行一部分项目
2.  里面包含Bootstrap中文网 http://www.bootcss.com/
3.  包含Js的框架，可以直接使用 在static里面的下载，常用的是mini
4.  可以自己创建项目以后，把代码拷贝进去或者直接项目的文件进去，不能直接运行