from django.db import models
from django.utils import timezone
from DjangoUeditor.models import UEditorField


# Create your models here.
class Product(models.Model):
    # 编号 标题 描述信息 价格  时间
    PRODUCTS_CHOICES = (('1', '家用机器人'), ('2', '智能监控'), ('3', '人脸识别解决方案'))
    id = models.AutoField('编号', primary_key=True)
    productType = models.CharField('产品类别', choices=PRODUCTS_CHOICES, max_length=50)
    title = models.CharField('标题', max_length=50)
    desc = models.TextField('介绍')
    price = models.DecimalField('价格', decimal_places=2, max_digits=7)
    pubDate = models.DateTimeField('发布日期', default=timezone.now)

    def __str__(self):
        return self.title

    class Meta:  # django 的后台
        managed = True
        verbose_name = '产品'
        verbose_name_plural = '产品'


# 图片(多张)
class ProductImg(models.Model):
    product = models.ForeignKey(Product, related_name='productImgs', verbose_name='产品', on_delete=models.CASCADE)
    photo = models.ImageField(upload_to='product/', blank=True, verbose_name='产品图片')

    class Meta:
        managed = True
        verbose_name = '产品图片'
        verbose_name_plural = '产品图片'


# 新闻
class News(models.Model):
    id = models.AutoField('编号', primary_key=True)
    NEW_CHOICES = (('1', '企业要闻'), ('2', '行业新闻'), ('3', '通知公告'))
    newType = models.CharField('产品类别', choices=NEW_CHOICES, max_length=50)
    title = models.CharField('标题', max_length=50)
    pubDate = models.DateTimeField('发布日期', default=timezone.now)
    content = UEditorField(u'内容', default='', width=1000, height=300, imagePath='news/images/', filePath='news/files/',
                           toolbars='mini', blank=True)

    # u'内容'用来定义该字段在后台管理系统中的别名
    # width，height:编辑器的宽度和高度，以像素为单位
    # imagePath:图片上传的路径,如"images/",实现上传到"{{MEDIA_ROOT}}/images"文件夹
    # filePath:附件上传的路径,如"files/",实现上传到"{{MEDIA_ROOT}}/files"文件夹
    # toolbars:配置你想显示的工具栏，取值为mini,normal,full,besttome, 代表小，一般，全部,涂伟忠贡献的一种样式。如果默认的工具栏不符合您的要求，您可以在settings里面配置自己的显示按钮。
    def __str__(self):
        return self.title

    class Meta:  # django 的后台
        managed = True
        verbose_name = '新闻'
        verbose_name_plural = '新闻'
