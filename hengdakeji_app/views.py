from django.shortcuts import render
from .models import Product, News
from django.core.paginator import Paginator


# Create your views here.
def home(request):
    return render(request, 'home.html')


def science(request):
    return render(request, 'science.html')


def about(request, type):
    # 不同的请求，返回同一个页面
    print(type)
    return render(request, "about.html", {"type": type})


def product(request, type):
    types = ['家用机器人', '智能监控', '人脸识别解决方案', ]
    # 根据产品的名字过滤
    result = Product.objects.filter(productType=type)
    # 分页操作
    paginator = Paginator(result, 2)
    # 获取前端页面的页码，如果没有传递则默认给1 通过这里 去改变网页
    num = request.GET.get("num", 1)
    # 根据页码获取数据
    page = paginator.page(num)

    return render(request, "product.html", {"type": type, "title": types[type - 1], "data": page})


def product_detail(request, id):
    # print("+++++++++",id)
    product_details = Product.objects.filter(id=id)

    if product_details:
        code = 1
        product_details = product_details[0]
    else:
        product_details = "没有查询到相关数据"
        code = 0

    return render(request, "product_detail.html", {"code": code, "data": product_details})


def service(request, type):
    return render(request, "service.html", {"type": type})


def news(request, type):
    types = ["企业要闻", "行业新闻", "通知公告"]
    result = News.objects.filter(newType=type)
    print(result[0].title)
    # 分页操作
    paginator = Paginator(result, 1) # 这里表示显示多少条数据
    # 获取前端页面的页码，如果没有传递则默认给1 通过这里 去改变网页
    num = request.GET.get("num", 1)
    # 根据页码获取数据
    page = paginator.page(num)
    return render(request, "news.html", {"type": type, "title": types[type - 1], "data": page})

def news_detail(request, news_id):
    # print("+++++++++",id)
    news_detail = News.objects.filter(id=news_id)

    if news_detail:
        code = 1
        news_details = news_detail[0]
    else:
        news_details = "没有查询到相关新闻"
        code = 0

    return render(request, "news_detail.html", {"code": code, "data": news_details})


def contact(request, type):
    return render(request, "contact.html", {"type": type})
