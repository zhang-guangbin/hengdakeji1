# Generated by Django 3.2.16 on 2022-10-19 08:31

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='编号')),
                ('productType', models.CharField(choices=[('1', '家用机器人'), ('2', '智能监控'), ('3', '人脸识别解决方案')], max_length=50, verbose_name='产品类别')),
                ('title', models.CharField(max_length=50, verbose_name='标题')),
                ('desc', models.TextField(verbose_name='介绍')),
                ('price', models.DecimalField(decimal_places=2, max_digits=7, verbose_name='价格')),
                ('pubDate', models.DateTimeField(default=django.utils.timezone.now, verbose_name='发布日期')),
            ],
            options={
                'verbose_name': '产品',
                'verbose_name_plural': '产品',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='ProductImg',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('photo', models.ImageField(blank=True, upload_to='product/', verbose_name='产品图片')),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='productImgs', to='hengdakeji_app.product', verbose_name='产品')),
            ],
            options={
                'verbose_name': '产品图片',
                'verbose_name_plural': '产品图片',
                'managed': True,
            },
        ),
    ]
