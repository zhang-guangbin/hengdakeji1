import qrcode
from qrcode.image.styledpil import StyledPilImage

qr = qrcode.QRCode(
    version=1, # 从 1 到 40 的整数，用于控制 QR 码的大小（最小的版本 1 是 21x21 矩阵）
    error_correction=qrcode.constants.ERROR_CORRECT_L,  # 控制用于 QR 码的纠错
    box_size=10,  # 控制QR码的每个“框”的像素数
    border=2, # 控制边框应为多少个框（默认值为 4，这是根据规范的最小值）
)
qr.add_data('https://www.baidu.com')
qr.make(fit=True)

# img = qr.make_image(back_color="white", fill_color=(55, 95, 35))
img = qr.make_image(image_factory=StyledPilImage,
                    embeded_image_path="F:\DjangoWorkSpace\hengdakeji\static\image\meigui.jpg") # 这个图片需要是正方形的，并且最好为白色底
img.save("baidu2.jpg")