from django.contrib import admin
from .models import Product, ProductImg,News

# Register your models here.
class ProductImgInline(admin.StackedInline):
    model = ProductImg
    extra = 1  # 默认显示条目的数量


# 定制整合产品和产品图片在一个页面管理
class ProductAdmin(admin.ModelAdmin):
    inlines = [ProductImgInline, ]


class NewAdmin(admin.ModelAdmin):
    style_fields = {'content': 'ueditor'}

admin.site.register(News, NewAdmin)

admin.site.register(Product, ProductAdmin)
