from django.apps import AppConfig


class HengdakejiAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'hengdakeji_app'