from django.db import models
from django.utils import timezone


# Create your models here.
class Student(models.Model):
    SEX_OPTION = (('m', '男'), ('f', '女'))
    id = models.AutoField('学号', primary_key=True) # primary_key 表示为主键
    name = models.CharField('姓名', unique=True, max_length=30,blank=False) #unique 该字段表示为唯一,blank = False 表示输入的值不能为空。
    sex = models.CharField('性别', choices=SEX_OPTION, max_length=6) # choices 表示为选择
    create_time = models.DateTimeField('注册时间', default=timezone.now)

    def __str__(self):
        return str(self.id) + '-' + self.name + '-' + self.sex

    class Meta:
        verbose_name = '学生'
        verbose_name_plural = verbose_name
