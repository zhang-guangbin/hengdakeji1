from django.apps import AppConfig


class DemoAppConfig(AppConfig):
    name = 'demo_app'
    verbose_name = '案例'
