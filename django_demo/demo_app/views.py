from django.shortcuts import render
from .models import Student


# Create your views here.
def index(request):
    # 查询所有学生信息
    data = Student.objects.all()
    return render(request, 'index.html', {'data': data})


def stu_reg(request):
    # 获取模板表单提交的数据
    name = request.POST.get("name")
    sex = request.POST.get("sex")

    # 入库
    Student.objects.create(name=name, sex=sex)

    # 查询所有学生信息
    data = Student.objects.all()

    return render(request, 'index.html', {'data': data})


def stu_del(request, id):
    # 根据ID删除学生信息
    Student.objects.filter(id=id).delete()

    # 查询所有学生信息
    data = Student.objects.all()

    return render(request, 'index.html', {'data': data})
